package ru.denimexe.bowstat.database.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table
@Data
@NoArgsConstructor
public class Competition implements DbEntity{

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    private Instant created;

    @Column
    private Instant day;

    @Builder(toBuilder = true)
    public Competition(Long id, String name, Instant created, Instant day) {
        this.id = id;
        this.day = day;
        this.name = name;
        this.created = created != null ? created : Instant.now();
    }
}
