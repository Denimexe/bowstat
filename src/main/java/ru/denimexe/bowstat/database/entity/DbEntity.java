package ru.denimexe.bowstat.database.entity;

import java.time.Instant;

public interface DbEntity {
    Long getId();
    Instant getCreated();
}
