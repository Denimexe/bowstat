package ru.denimexe.bowstat.database.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table
@Data
@NoArgsConstructor
public class Participator implements DbEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    private Instant created;

    @Builder(toBuilder = true)
    public Participator(Long id, String name, Instant created) {
        this.id = id;
        this.name = name;
        this.created = created != null ? created : Instant.now();
    }
}
