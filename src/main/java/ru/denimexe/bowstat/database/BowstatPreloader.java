package ru.denimexe.bowstat.database;

import javafx.application.Preloader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ru.denimexe.bowstat.SpringBootFxApplication;

/**
 * Класс окна предзагрузчика для приложения
 */

public class BowstatPreloader extends Preloader {

    private Stage stage;
    private ProgressIndicator progress;
    private final Integer HEIGHT = 300;
    private final Integer WIDTH = 300;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        stage.initStyle(StageStyle.UNDECORATED);
        Group root = new Group();
        root.setTranslateX(0);
        root.setTranslateY(0);
        Scene scene = new Scene(root, WIDTH, HEIGHT, Color.color(0.66, 0.1406, 0.1328));

        StackPane pane = new StackPane();
        pane.setPrefSize(WIDTH, HEIGHT);
        pane.setPadding(new Insets(20));

        Label loadingLabel = new Label("Загрузка...");
        loadingLabel.setStyle("-fx-text-fill: #fff; -fx-font-weight: 800; -fx-font-size: 12pt; -fx-font-family: 'Calibri'");

        ImageView loadingImage = new ImageView();
        loadingImage.setImage(new Image(SpringBootFxApplication.class.getResourceAsStream("/images/app-icon_lg_reverse.png")));
        loadingImage.setFitWidth(WIDTH - 50);
        loadingImage.setFitHeight(HEIGHT - 50);


        progress = new ProgressIndicator();
        progress.setStyle("-fx-font:bold 20pt Arial; -fx-progress-color: #fff");
        pane.getChildren().add(loadingImage);
        pane.getChildren().add(progress);
        pane.getChildren().add(loadingLabel);
        pane.setAlignment(progress, Pos.CENTER);
        pane.setAlignment(loadingImage, Pos.CENTER);
        pane.setAlignment(loadingLabel, Pos.CENTER);

        //setCenter(progress);

        root.getChildren().addAll(pane);

        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void handleProgressNotification(ProgressNotification pn) {
        progress.setProgress(pn.getProgress());
    }

    @Override
    public void handleApplicationNotification(PreloaderNotification info) {
        ProgressNotification ntf = (ProgressNotification) info;
        if (ntf.getProgress() == 1.0)
            stage.hide();
        else progress.setProgress(-1);
    }
}
