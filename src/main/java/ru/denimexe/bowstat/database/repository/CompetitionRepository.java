package ru.denimexe.bowstat.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.denimexe.bowstat.database.entity.Competition;
import ru.denimexe.bowstat.database.entity.Participator;

public interface CompetitionRepository extends JpaRepository<Competition, Long> {
}
