package ru.denimexe.bowstat.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.denimexe.bowstat.database.entity.Participator;
import ru.denimexe.bowstat.database.repository.ParticipatorRepository;
import ru.denimexe.bowstat.model.dto.EntityPage;
import ru.denimexe.bowstat.model.dto.entity.ParticipatorDto;
import ru.denimexe.bowstat.model.mapper.ParticipatorMapper;

import java.util.stream.Collectors;

import static ru.denimexe.bowstat.model.dto.entity.DbEntityDto.PAGE_SIZE;

@Slf4j
@Service
@AllArgsConstructor
public class ParticipatorService {

    private final ParticipatorRepository repository;
    private final ParticipatorMapper mapper = ParticipatorMapper.Instance;

    public EntityPage<ParticipatorDto> getPage() {
        return getPage(null);
    }

    public EntityPage<ParticipatorDto> getPage(Integer page) {
        Page<Participator> competitionPage = repository.findAll(PageRequest.of(page != null ? page : 0, PAGE_SIZE));
        EntityPage<ParticipatorDto> result = new EntityPage<>();
        result.setData(competitionPage.getContent().stream().map(mapper::entityToDto).collect(Collectors.toList()));
        result.setPageNum(competitionPage.getPageable().getPageNumber());
        result.setTotalPages(competitionPage.getTotalPages());

        log.debug("result page: {}", result);
        return result;
    }

    public void save(ParticipatorDto dto) {
        // FIXME validation
        Participator competition = mapper.dtoToEntity(dto);
        repository.save(competition);
    }
}
