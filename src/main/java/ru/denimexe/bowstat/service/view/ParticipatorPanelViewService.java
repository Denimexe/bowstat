package ru.denimexe.bowstat.service.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import ru.denimexe.bowstat.controller.ParticipatorPanelController;
import ru.denimexe.bowstat.controller.ParticipatorPopupController;
import ru.denimexe.bowstat.model.dto.entity.ParticipatorDto;

@Slf4j
@Service
public class ParticipatorPanelViewService {

    @Autowired
    private ParticipatorPanelController controller;

    @Value("classpath:/fxml/participatorPanel.fxml")
    Resource fxml;

    public void showDialog() {
        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(fxml.getURL());
            loader.setController(controller);

            VBox page = loader.load();

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.initStyle(StageStyle.UTILITY);

            dialogStage.initModality(Modality.WINDOW_MODAL);
            Scene scene = new Scene(page);
//            scene.getStylesheets().add(BowstatApplication.class.getResource("/css/style.css").toString());
            dialogStage.setScene(scene);
            dialogStage.centerOnScreen();

            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogStage.showAndWait();

            // нельзя убирать, иначе повторно окно не открывается
            scene.setRoot(new AnchorPane());
            dialogStage.close();
        } catch (Exception ex) {
            log.error("showDialog()", ex);
            // FIXME добавить нормальное исключение
            throw new InternalException("");
        }
    }
}
