package ru.denimexe.bowstat.service.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import ru.denimexe.bowstat.controller.AcknowledgePopupController;

@Slf4j
@Service
public class AcknowledgePopupViewService {

    @Autowired
    private AcknowledgePopupController controller;

    @Value("classpath:/fxml/acknowledgePopup.fxml")
    Resource fxml;

    public boolean showDialog(String header, String msg) {
        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(fxml.getURL());
            loader.setController(controller);

            VBox page = loader.load();

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.initStyle(StageStyle.UTILITY);

            dialogStage.setTitle(header);

            dialogStage.initModality(Modality.APPLICATION_MODAL);
            Scene scene = new Scene(page);
//            scene.getStylesheets().add(BowstatApplication.class.getResource("/css/style.css").toString());
            dialogStage.setScene(scene);
            dialogStage.centerOnScreen();
            controller.setMessage(msg);

            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogStage.showAndWait();

            // нельзя убирать, иначе повторно окно не открывается
            scene.setRoot(new AnchorPane());
            dialogStage.close();

            return controller.isOk();
        } catch (Exception ex) {
            log.error("showDialog()", ex);
            // FIXME добавить нормальное исключение
            return false;
        }
    }
}
