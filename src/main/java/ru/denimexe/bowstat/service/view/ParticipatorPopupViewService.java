package ru.denimexe.bowstat.service.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import ru.denimexe.bowstat.controller.ParticipatorPopupController;
import ru.denimexe.bowstat.model.dto.entity.ParticipatorDto;

@Slf4j
@Service
public class ParticipatorPopupViewService {

    @Autowired
    private ParticipatorPopupController controller;

    @Autowired
    private AcknowledgePopupViewService acknowledgeViewService;

    @Value("classpath:/fxml/competitionPopup.fxml")
    Resource fxml;

    public ParticipatorDto showDialog() {
        return showDialog(null);
    }

    public ParticipatorDto showDialog(ParticipatorDto entity) {
        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(fxml.getURL());
            loader.setController(controller);

            VBox page = loader.load();

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.initStyle(StageStyle.UNDECORATED);

            dialogStage.initModality(Modality.APPLICATION_MODAL);
            Scene scene = new Scene(page);
//            scene.getStylesheets().add(BowstatApplication.class.getResource("/css/style.css").toString());
            dialogStage.setScene(scene);
            dialogStage.centerOnScreen();
            controller.setParticipator(entity != null ? entity : new ParticipatorDto());
            controller.setStageAndScene(dialogStage, scene);

            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogStage.showAndWait();


            // нельзя убирать, иначе повторно окно не открывается
            scene.setRoot(new AnchorPane());
            dialogStage.close();

            return controller.isSave() ? controller.getParticipator() : null;
        } catch (Exception ex) {
            log.error("showDialog()", ex);
            // FIXME добавить нормальное исключение
            throw new InternalException("");
        }
    }
}
