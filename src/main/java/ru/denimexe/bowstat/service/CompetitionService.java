package ru.denimexe.bowstat.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.denimexe.bowstat.database.entity.Competition;
import ru.denimexe.bowstat.database.repository.CompetitionRepository;
import ru.denimexe.bowstat.model.dto.entity.CompetitionDto;
import ru.denimexe.bowstat.model.dto.EntityPage;
import ru.denimexe.bowstat.model.mapper.CompetitionMapper;

import java.util.stream.Collectors;

import static ru.denimexe.bowstat.model.dto.entity.DbEntityDto.PAGE_SIZE;

@Slf4j
@Service
@AllArgsConstructor
public class CompetitionService {

    private final CompetitionRepository repository;
    private final CompetitionMapper mapper = CompetitionMapper.Instance;

    public EntityPage<CompetitionDto> getPage() {
        return getPage(null);
    }

    public EntityPage<CompetitionDto> getPage(Integer page) {
        Page<Competition> competitionPage = repository.findAll(PageRequest.of(page != null ? page : 0, PAGE_SIZE));
        EntityPage<CompetitionDto> result = new EntityPage<>();
        result.setData(competitionPage.getContent().stream().map(mapper::entityToDto).collect(Collectors.toList()));
        result.setPageNum(competitionPage.getPageable().getPageNumber());
        result.setTotalPages(competitionPage.getTotalPages());

        log.debug("result page: {}", result);
        return result;
    }

    public void save(CompetitionDto dto) {
        // FIXME validation
        Competition competition = mapper.dtoToEntity(dto);
        repository.save(competition);
    }
}
