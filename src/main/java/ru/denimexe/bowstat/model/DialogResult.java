package ru.denimexe.bowstat.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DialogResult {
    boolean isOk;
}
