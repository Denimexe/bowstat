package ru.denimexe.bowstat.model.dto.entity;

import com.sun.istack.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
public class ParticipatorDto extends DbEntityDto {
    @NotNull
    private String name;

    @Builder(toBuilder = true)
    public ParticipatorDto(@NotNull Long id, @NotNull String name, Instant created) {
        super(id, created);
        this.name = name;
    }
}
