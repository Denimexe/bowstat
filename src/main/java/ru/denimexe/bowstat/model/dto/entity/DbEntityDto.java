package ru.denimexe.bowstat.model.dto.entity;

import com.sun.istack.NotNull;
import lombok.Data;

import java.time.Instant;

/**
 * Общий интерфейс объекта сущности БД
 */
@Data
public abstract class DbEntityDto {

    public static final int PAGE_SIZE = 100;

    Long id;
    @NotNull
    Instant created;

    public DbEntityDto() {
        this.created = Instant.now();
    }

    public DbEntityDto(Long id, Instant created) {
        this.id = id;
        this.created = created != null ? created : Instant.now();
    }
}
