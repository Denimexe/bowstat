package ru.denimexe.bowstat.model.dto.entity;

import com.sun.istack.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import java.time.Instant;

@Data
@NoArgsConstructor
@Validated
public class CompetitionDto extends DbEntityDto {

    @NotNull
    private String name;
    @NotNull
    private Instant day;

    @Builder(toBuilder = true)
    public CompetitionDto(Long id, @NotNull String name, Instant created, @NotNull Instant day) {
        super(id, created);
        this.day = day;
        this.name = name;
    }
}
