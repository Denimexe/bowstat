package ru.denimexe.bowstat.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.denimexe.bowstat.model.dto.entity.DbEntityDto;

import java.util.Collection;

@Data
@NoArgsConstructor
public class EntityPage<T extends DbEntityDto> {
    Collection<T> data;
    int pageNum;
    int totalPages;
}
