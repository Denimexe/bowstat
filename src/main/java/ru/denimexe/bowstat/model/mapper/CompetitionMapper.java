package ru.denimexe.bowstat.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.denimexe.bowstat.database.entity.Competition;
import ru.denimexe.bowstat.model.dto.entity.CompetitionDto;

@Mapper
public interface CompetitionMapper {

    CompetitionMapper Instance = Mappers.getMapper(CompetitionMapper.class);

    CompetitionDto entityToDto(Competition competition);

    Competition dtoToEntity(CompetitionDto competition);
}
