package ru.denimexe.bowstat.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.denimexe.bowstat.database.entity.Participator;
import ru.denimexe.bowstat.model.dto.entity.ParticipatorDto;

@Mapper
public interface ParticipatorMapper {

    ParticipatorMapper Instance = Mappers.getMapper(ParticipatorMapper.class);

    ParticipatorDto entityToDto(Participator participator);

    Participator dtoToEntity(ParticipatorDto participator);
}
