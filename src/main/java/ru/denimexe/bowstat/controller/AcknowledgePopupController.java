package ru.denimexe.bowstat.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import lombok.Data;
import org.springframework.stereotype.Service;

@Service
@Data
public class AcknowledgePopupController {

    private boolean isOk = false;
    private String message;

    @FXML
    public Label messageLabel;
    @FXML
    public Button okBtn;
    @FXML
    public Button cancelBtn;

    @FXML
    public void initialize() {
        okBtn.setOnAction(action -> {
            isOk = true;
            Stage stage = (Stage) cancelBtn.getScene().getWindow();
            stage.close();
        });

        cancelBtn.setOnAction(action -> {
            isOk = false;
            Stage stage = (Stage) cancelBtn.getScene().getWindow();
            stage.close();
        });
    }

    public void setMessage(String msg) {
        this.message = msg;
        messageLabel.setText(msg);
    }

}