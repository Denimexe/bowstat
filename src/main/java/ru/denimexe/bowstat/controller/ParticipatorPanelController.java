package ru.denimexe.bowstat.controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.bowstat.model.dto.EntityPage;
import ru.denimexe.bowstat.model.dto.entity.ParticipatorDto;
import ru.denimexe.bowstat.service.ParticipatorService;
import ru.denimexe.bowstat.service.view.ParticipatorPopupViewService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ParticipatorPanelController {

    @Autowired
    private ParticipatorService participatorService;

    @Autowired
    private ParticipatorPopupViewService participatorViewService;

    private Stage stage;
    private Scene scene;

    @FXML
    public Button createBtn;
    @FXML
    public Label lbl;
    @FXML
    public ListView<String> list;

    @FXML
    public void initialize() {

        loadDb();

        createBtn.setOnAction(actionEvent -> {
            try {

                ParticipatorDto participator = participatorViewService.showDialog();
                if (participator != null) {
                    participatorService.save(participator);
                    loadDb();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void setStageAndScene(Stage stage, Scene scene) {
        this.stage = stage;
        this.scene = scene;

        scene.setOnKeyPressed(t -> {
            KeyCode key = t.getCode();
            if (key == KeyCode.ESCAPE) {
                scene.setRoot(new AnchorPane());
                stage.close();
            }
        });
    }

    private void loadDb() {
        EntityPage<ParticipatorDto> participators = participatorService.getPage();
        List<String> names = participators.getData().stream().map(ParticipatorDto::getName).collect(Collectors.toList());
        list.setItems(
                FXCollections.observableArrayList(names)
        );
    }
}