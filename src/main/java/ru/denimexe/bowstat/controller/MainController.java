package ru.denimexe.bowstat.controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.bowstat.model.dto.EntityPage;
import ru.denimexe.bowstat.model.dto.entity.CompetitionDto;
import ru.denimexe.bowstat.service.CompetitionService;
import ru.denimexe.bowstat.service.view.CompetitionPopupViewService;
import ru.denimexe.bowstat.service.view.ParticipatorPanelViewService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MainController {

    @Autowired
    private CompetitionService competitionService;

    @Autowired
    private CompetitionPopupViewService competitionViewService;

    @Autowired
    private ParticipatorPanelViewService participatorPanelService;

    @FXML
    public Button btn;
    @FXML
    public Label lbl;
    @FXML
    public ListView<String> list;
    @FXML
    private Button showUserPanelBtn;

    @FXML
    public void initialize() {

        loadDb();

        btn.setOnAction(actionEvent -> {
            try {

                CompetitionDto competition = competitionViewService.showDialog();
                if (competition != null) {
                    competitionService.save(competition);
                    loadDb();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        showUserPanelBtn.setOnAction(actionEvent -> {
            participatorPanelService.showDialog();
        });
    }

    private void loadDb() {
        EntityPage<CompetitionDto> competitions = competitionService.getPage();
        List<String> names = competitions.getData().stream().map(CompetitionDto::getName).collect(Collectors.toList());
        list.setItems(
                FXCollections.observableArrayList(names)
        );
    }
}