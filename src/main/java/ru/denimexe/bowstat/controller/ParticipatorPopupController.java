package ru.denimexe.bowstat.controller;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.denimexe.bowstat.model.dto.entity.ParticipatorDto;
import ru.denimexe.bowstat.service.view.AcknowledgePopupViewService;

import java.util.HashMap;
import java.util.Map;


// FIXME добавить сохранение внутрь контроллера без возврата Dto
@Service
@Data
public class ParticipatorPopupController {

    private ParticipatorDto participator;

    private boolean save = false;
    private boolean edited = false;

    private Stage stage;
    private Scene scene;

    private Map<String, Boolean> validationMap = new HashMap<>();

    @Autowired
    private AcknowledgePopupViewService acknowledgeViewService;

    @FXML
    public Button cancelBtn;
    @FXML
    public Button saveBtn;
    @FXML
    public TextField nameTField;
    @FXML
    public Label nameErrorLabel;


    @FXML
    public void initialize() {

        save = false;

        validationMap.put("nameTField", participator != null && validateString(participator.getName()));

        cancelBtn.setOnAction(action -> {
            close();
        });

        saveBtn.setOnAction(action -> {
            save = true;
            close();
        });

        nameTField.textProperty().addListener(
                (observable, oldValue, newValue) -> {
                    getParticipator().setName(newValue);
                    edited = true;
                }
        );
    }

    public void setStageAndScene(Stage stage, Scene scene) {
        this.stage = stage;
        this.scene = scene;

        scene.setOnKeyPressed(t -> {
            KeyCode key = t.getCode();
            if (key == KeyCode.ESCAPE) {
                closeViewWithoutSaving();
            }
        });
    }

    private void close() {
        if (save) {
            validate();
            if (checkValidation()) {
                Stage stage = (Stage) cancelBtn.getScene().getWindow();
                stage.close();
            }
        } else {
            Stage stage = (Stage) cancelBtn.getScene().getWindow();
            stage.close();
        }
    }

    private void validate() {
        nameTField.setStyle("-fx-background-color: white");
        nameErrorLabel.setVisible(false);
        boolean isValid = validateString(participator.getName());
        validationMap.put("nameTField", isValid);
        if (!isValid) {
            nameTField.setStyle("-fx-background-color: red");
            nameErrorLabel.setVisible(true);
            nameErrorLabel.setText("Ошибка");
        }
    }

    private boolean validateString(String str) {
        return StringUtils.isNotBlank(str);
    }

    private boolean checkValidation() {
        return validationMap.values().stream().reduce(true, (a, b) -> a & b);
    }

    private void closeViewWithoutSaving() {
        if (!edited) {
            scene.setRoot(new AnchorPane());
            stage.close();
        } else {
            boolean acknowledge = acknowledgeViewService.showDialog("Отменить изменения?",
                    "Вы уверены, что хотите отменить изменения? Все несохраненные данные будут потеряны!");
            if (acknowledge) {
                scene.setRoot(new AnchorPane());
                stage.close();
            }
        }
    }

}